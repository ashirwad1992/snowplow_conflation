import geopandas as gd
from dateutil.parser import parse as dateparser
from shapely.geometry import Point,LineString
import pyproj
from functools import partial
from shapely.ops import transform
import pandas as pd
import datetime as dt
from multiprocessing import Pool
import os

# Global Variables
raw_data_root = ''
conflated_data_root = ''
output_data_root = ''
tdf = pd.read_csv('trucks.csv',header=None)
trucks = [truck for truck in tdf[0]]
chunk_size = 10**4
num_processes = 4

# Spatial Transform Configuration
project = partial(
    pyproj.transform,
    pyproj.Proj(init='EPSG:4326'),
    pyproj.Proj(init='EPSG:26986'))


# Date Parser function
def parse_date(df):
    return int(dateparser(df['LOGDT']).strftime('%Y%m%d%H%M%S'))


def distance_travelled_by_lat_long(df,label):
    df = df.loc[df['LABEL']==label]
    df = df.sort_values(by=['LOGDT_LOCAL'])
    if len(df) < 2:
        print('Empty dataframe')
        return 0.0
    # Spatial Functions
    line = LineString(df[['XPOSITION','YPOSITION']].values)
    line2 = transform(project, line)
    length = line2.length * 0.000621371
    print(length, 'Miles')
    return length


def distance_traveled_from_measures(df,label):
    tdf = df.loc[df['LABEL']==label]
    tdf = tdf.sort_values(by=['LOGDT_LOCAL'])
    return abs(tdf['measure']-tdf['END_MEASURE']).sum()


def snowplow_process_accuracy(rdf, pdf):
    odf = pd.DataFrame(columns=['Raw Miles', 'Processed Miles', 'Error'])
    for truck in trucks:
        rm = distance_travelled_by_lat_long(rdf,truck)
        pm = distance_traveled_from_measures(pdf,truck)
        error = ((rm-pm)*100)/rm
        odf.loc[truck] = [rm,pm,error]
    print('done')
    return odf.reset_index(drop=False)


def run_snowplow_accuracy(date):
    # Initialization
    datestr = date.strftime('%m-%d-%Y')
    print('Started processing for', datestr)
    monthstr = date.strftime('%Y-%m')
    raw_file = os.path.join(raw_data_root,monthstr,datestr,'snowplow.csv')
    conf_file = os.path.join(conflated_data_root,monthstr,datestr + '_filtered.csv')
    dest_folder = os.path.join(output_data_root,monthstr)
    if not os.path.exists(dest_folder):
        print('Created folder', dest_folder)
        os.makedirs(dest_folder)
    dest_file = os.path.join(dest_folder, datestr+'_measures.csv')

    raw_itr = pd.read_csv(raw_file,itertor=True,chunksize=chunk_size)
    # Raw Data filtered by trucks
    rdf = pd.concat([chunk.loc[chunk['LABEL'].isin(trucks)]] for chunk in raw_itr)
    rdf['LOGDT_LOCAL'] = rdf.apply(lambda row: parse_date(row), axis=1)

    # Conflated data
    cdf = pd.read_csv(conf_file)
    odf = snowplow_process_accuracy(rdf,cdf)
    odf.fillna(0).rename(columns={'index': 'Label'}).to_csv(dest_file, index=False)
    print('Completed processing for', date)



def main():
    start_date = dt.date(2016, 10, 1)
    end_date = dt.date(2018,7,31)
    date_increment = dt.timedelta(days=1)
    dates = []
    while start_date <= end_date:
        dates.append(start_date)
        start_date += date_increment

    with Pool(num_processes) as pool:
        pool.map(run_snowplow_accuracy,dates)
    print('Completed')


if __name__ == '__main__':
    main()




