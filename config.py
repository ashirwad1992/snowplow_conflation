
def buildDataSource(host,port,db,username,password):
    return {
        'host': host,
        'port': port,
        'db': db,
        'username': username,
        'password': password
    }

#MySql Connection Properties
skyhawk_datasource = buildDataSource('dbus.connectanywhere.co',3306,'iowadot','iowadot3','M5m5h567s8')
#Postgres connection properties
pg_datasource = buildDataSource('localhost',5432,'lrs','postgres','root')
