-- noinspection SqlNoDataSourceInspectionForFile

-- Create Table with SRID 26986 SRID and add index
DROP TABLE IF EXISTS lrs.lrs_geom;
CREATE TABLE lrs.lrs_geom as
SELECT objectid,effective_start_date,effective_end_date, 
	geographic_identifier, system_code, route_number, direction, ramp_sequence, route_id,
	ST_Transform(geom,26986) AS geom
FROM lrs.lrs WHERE effective_end_date is null;

-- Merge Multi Line Strings into line strings.
DROP function IF EXISTS lrs.split_multi_lines();
CREATE OR REPLACE function lrs.split_multi_lines() RETURNS VOID AS
$BODY$
DECLARE
r RECORD;
g geometry;
BEGIN
FOR r IN SELECT * FROM lrs.lrs_geom where GeometryType(geom) ='MULTILINESTRING' LOOP
	FOR g IN SELECT (t.d).geom FROM (SELECT ST_Dump(r.geom) AS d) AS t LOOP
	    INSERT INTO lrs.lrs_geom(objectid,effective_start_date,effective_end_date,
	            geographic_identifier, system_code, route_number, direction,
	            ramp_sequence, route_id,geom)
	    VALUES (r.objectid,r.effective_start_date,r.effective_end_date,
	            r.geographic_identifier, r.system_code, r.route_number,
	            r.direction, r.ramp_sequence, r.route_id,g);
	END LOOP;
END LOOP;
DELETE FROM lrs.lrs_geom WHERE GeometryType(geom) ='MULTILINESTRING';
END
$BODY$
language plpgsql;

-- Split and remove multi-line string.
SELECT lrs.split_multi_lines();

DROP INDEX IF EXISTS lrs.lrs_geom_nat_idx;
CREATE INDEX lrs_geom_nat_idx
ON lrs.lrs_geom USING GIST(geom);

DROP INDEX IF EXISTS lrs.lrs_geom_route_id;
CREATE INDEX lrs_geom_route_id
ON lrs.lrs_geom (route_id);

CLUSTER lrs_geom_nat_idx ON lrs.lrs_geom;

-- Create geometry to measure function
DROP FUNCTION IF EXISTS lrs.geom_to_measure(text,int,int,text,text);

CREATE OR REPLACE FUNCTION lrs.geom_to_measure(loc text, srid int, tolerance int, rid text default null,
											dir text default null )
 RETURNS TABLE (
	routeId varchar(31),
	measure double precision,
	distance double precision
 ) AS
 $func$
 BEGIN
 RETURN QUERY
 WITH p(point) AS (values (ST_Transform(ST_GeomFromText(loc,srid),26986)))
 SELECT route_id routeId, 
		ST_InterpolatePoint(geom, p.point)  measure,
		ST_Distance(geom,p.point) distance
		FROM lrs.lrs_geom g, p
		WHERE (rid is null OR g.route_id = rid)
			AND (dir is null OR g.direction = dir)
			AND ST_DWithIn(g.geom,p.point,tolerance)
			;


 END
 $func$ LANGUAGE 'plpgsql';

--Test
-- SELECT lrs.geom_to_measure('POINT(-91.0358200073 41.4266395569)',4326,50,'0038',null);
select lrs.geom_to_measure('POINT(-91.57308959999999 41.29034042)',4326,50,'S001930092W',null)