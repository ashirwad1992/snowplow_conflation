import time, logging,os
import datetime as dt
import pandas as pd
from snowplowapp.utils import lrsprocess
from multiprocessing import Pool

# Logging configuration
logger = logging.getLogger('SnowPlow_Process')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('run_sp_mar_2018.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)



# Configuration
root_folder_path = r"C:\Users\krishnaj\Box Sync\Snowplow Data"
# root_folder_path = r"C:\Users\!krishnaj\work\Snowplow\Test\Data\Raw"
# dest_folder_path = r"C:\Users\!krishnaj\work\Snowplow\Test\Data\Raw"
dest_folder_path = r"C:\Users\krishnaj\Box Sync\Snowplow_Processed"
chunk_size =10**4
num_processes = 4

# Global vars
total_pings = 0
start = time.time()


def process_file(date):
    global total_pings
    datestr = date.strftime('%m-%d-%Y')
    logger.debug('Processing initialized for the %s file', datestr)
    file_path = os.path.join(root_folder_path, datestr, 'snowplow.csv')
    if os.path.exists(file_path):
        num_pings = 0
        startmp = time.time()
        dest_file_path = os.path.join(dest_folder_path, datestr + '_processed.csv')
        for chunk in pd.read_csv(file_path, header=0, chunksize=chunk_size):
            num_pings += len(chunk)
            pdf = lrsprocess.process_snowplow(chunk)
            logger.debug('Appending processed output of len %d at %s', len(pdf), dest_file_path)
            pdf.to_csv(dest_file_path, index=False, mode='a', header=(not os.path.exists(dest_file_path)))
            logger.info('File: %s - Number of pings processed: %d in time %s seconds', datestr, num_pings,
                        time.time() - startmp)
        total_pings += num_pings
        logger.info('Finished processing for %s file with %d pings.', datestr,num_pings)
        logger.info('Total pings processed so far: %d, And total time taken so far %s seconds', total_pings,
                    (time.time() - start))
    else:
        logger.info('snow plow data not available for %s', datestr)


def main():
    start_date = dt.date(2018, 3, 12)
    end_date = dt.date(2018, 3,16)
    date_increment = dt.timedelta(days=1)
    dates = []
    while start_date <= end_date:
        dates.append(start_date)
        start_date += date_increment

    with Pool(num_processes) as pool:
        pool.map(process_file,dates)


    logger.info("Completed!!!!")
    logger.info('Total elapsed time = %s seconds ', time.time() - start)


if __name__ == '__main__':
    main()
