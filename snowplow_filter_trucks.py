import pandas as pd
import os
import datetime as dt
from multiprocessing import Pool

root_folder_path = r"C:\Users\!krishnaj\work\Snowplow\LRS_Conflation\Output\2016-11"
dest_folder_path = r"C:\Users\!krishnaj\work\Snowplow\LRS_Conflation\Output\Filtered"
chunk_size = 10**4
num_processes = 4


# Trucks label
df = pd.read_csv('trucks.csv',header=None)
trucks = [truck for truck in df[0]]

def process_file(date):
    total_pings = 0
    datestr = date.strftime('%m-%d-%Y')
    src_file_path = os.path.join(root_folder_path, datestr + '_processed.csv')
    dest_file_path = os.path.join(dest_folder_path, datestr + '_filtered.csv')
    print('started processing for ', src_file_path)
    if os.path.exists(src_file_path):
        for chunk in pd.read_csv(src_file_path, header=0, chunksize=chunk_size):
            total_pings += len(chunk)
            tdf = chunk.loc[chunk['LABEL'].isin(trucks)]
            tdf.to_csv(dest_file_path, index=False, mode='a', header=(not os.path.exists(dest_file_path)))
            print(datestr, ' - Total pings processed: ', total_pings)
    else:
        print('No file exist for ', src_file_path)


def main():
    start_date = dt.date(2016, 11, 1)
    end_date = dt.date(2016,11,30)
    date_increment = dt.timedelta(days=1)
    dates = []
    while start_date <= end_date:
        dates.append(start_date)
        start_date += date_increment

    with Pool(num_processes) as pool:
        pool.map(process_file,dates)
    print('Completed')


if __name__ == '__main__':
    main()