from flask import Flask, jsonify, make_response,request,redirect,Response
import pandas as pd
from snowplowapp.utils import dbaccess, lrsprocess

app = Flask(__name__)

app.config['MAX_CONTENT_LENGTH'] = 5 * 1024 * 1024

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in set(['csv'])

# def get_db():
#     """Opens a new database connection if there is none yet for the
#        current application context.
#        """
#     if not hasattr(g, 'db'):
#         g.db = connect_db()
#     return g.db
#
#
#
# def connect_db():
#     """Opens a new database connection"""
#     return psycopg2.connect(host="localhost", dbname="lrs",user="postgres", password="root")

# @app.teardown_appcontext
# def close_db(error):
#     """Closes the database again at the end of the request."""
#     if hasattr(g, 'db'):
#         g.db.close()

@app.route('/lrs/api/v1/geometrytomeasure', methods = ['POST'])
def getMeasures():
    req = request.json
    locations = list(map(lambda t: [t['geometry']['x'],t['geometry']['y'],
                                    t['routeId'].upper() if 'routeId' in t else None,
                                    t['direction'].upper() if 'direction' in t else None],
                         req['locations']))
    print(locations)
    tolerance = int(req['tolerance'])
    srid = int(req['inSR'])
    results = dbaccess.geometryToMeasures(locations,tolerance,srid)
    return make_response(jsonify(results),200)

@app.route('/snowplow/api/v1/process', methods = ['POST'])
def snowplow_process():
    if 'file' not in request.files:
        return Response('No file uploaded', status=400)
    f = request.files['file']
    if f.name == '':
        return Response('No filename provided', status=400)
    if f:
        df = pd.read_csv(f,header=0)
        df = lrsprocess.process_snowplow(df)
        resp = make_response(df.to_csv())
        resp.headers["Content-Disposition"] = "attachment; filename=processed.csv"
        resp.headers["Content-Type"] = "text/csv"
        return resp
    else:
        return Response('Invalid file provided',status=400)




# def geometryToMeasures(locations, tolerance,srid):
#     points = [('POINT({} {})'.format(x[0],x[1]),x[2],x[3]) for x in locations]
#     cur = get_db().cursor()
#     results = []
#     print(points,srid,tolerance)
#     keys = ['routeId', 'measure', 'distance']
#     for p in points:
#         cur.callproc('lrs.geom_to_measure', (p[0], srid, tolerance,p[1],p[2]))
#         records = cur.fetchall()
#         print(records)
#         results.append([dict(zip(keys,row)) for row in records])
#     cur.close()
#     return results

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    app.run()
