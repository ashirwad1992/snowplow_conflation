import psycopg2
from config import pg_datasource as pgd

db_connections ={}
cursors = {}

def get_pgis_db():
    """Opens a new database connection if there is none yet for the
       current application context.
       """
    if 'pgis' not in db_connections:
       db_connections['pgis'] = connect_pgis_db()
    return db_connections['pgis']

def connect_pgis_db():
    """Opens a new database connection"""
    print("Connecting to post gis database")
    return psycopg2.connect(host=pgd['host'], dbname=pgd['db'],user=pgd['username'], password=pgd['password'])

def close_pgis_db(error):
    """Closes the database again at the end of the request."""
    if 'pgis' in db_connections:
        db_connections['pgis'].close()
        del db_connections['pgis']

def get_pgis_cursor():
    if ('pgis' not in cursors) or (cursors['pgis'].closed):
        cursors['pgis'] = get_pgis_db().cursor()
    return cursors['pgis']

def geometryToMeasures(locations, tolerance,srid):
    points = [('POINT({} {})'.format(x[0],x[1]),x[2],x[3]) for x in locations]
    cur = get_pgis_cursor()

    results = []
    # print(points,srid,tolerance)
    keys = ['routeId', 'measure', 'distance']
    for p in points:
        try:
            cur.callproc('lrs.geom_to_measure', (p[0], srid, tolerance,p[1],p[2]))
            records = cur.fetchall()
            # print(records)
            results.append([dict(zip(keys,row)) for row in records])
        except:
            results.append([])
            print('Error in querying the point', p[0])
    cur.close()
    return results

def check_route_id_exists(routeId):
    cur = get_pgis_cursor()
    cur.execute("select exists (select 1 from lrs.lrs_geom where route_id = %s)", (routeId,))
    r =  cur.fetchone()
    return r[0]
