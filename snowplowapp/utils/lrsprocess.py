import snowplowapp.utils.dbaccess as dbaccess
import pandas as pd
from dateutil.parser import parse as dateparser

# Route Descriptor - High priority
route_desc_priority = {'S':1,'C':2,'M':3,'P':4,'I':5}
# System Code - Medium Priority
route_code_priority = {'1': 1,'2':2,'3':3,'4':4}
# Direction - Low Priority
route_dir_priority = {'N': 1, 'E': 1, 'W': 2, 'S': 2}

rc = 0
dc = 0

def increment_rc():
    global rc
    rc +=1
def increment_dc():
    global dc
    dc += 1

def dominant_route_selector(route_id):
    # Exceptional case - Give these routes high priority.
    if route_id == 'S001910080W' or route_id== 'S001910080E':
        return 1
    # Route Criteria =  route_desc > route_code > route_number > route_dir > ramp
    key = int('{}{}{}{}'.format(route_desc_priority[route_id[0]],route_code_priority[route_id[5]],route_id[6:10],route_dir_priority[route_id[10]]))
    if len(route_id) > 11:
        # If it is a RAMP, add extra weight
        key += 5
    return key

def parseDate(df):
    return int(dateparser(df['LOGDT']).strftime('%Y%m%d%H%M%S'))


def process_snowplow(df):
    global rc, dc
    '''
    Process snow plow data by adding accurate route ids and measures.
    :param df:
    :return:
    '''
    # Step 1 - Group by label
    # Step 1 - Find routeId and measures for all locations
    locations = [x.tolist()+[None]*2 for x in df[['XPOSITION','YPOSITION']].values]
    gm = dbaccess.geometryToMeasures(locations, 50, 4326)

    # Step 2 - Update dataframe with accurate routeIds and measures
    rm = [get_routeId(x) for x in gm]
    df['routeId'] = [x[0] for x in rm]
    df['measure'] = [x[1] for x in rm]
    df['LOGDT_LOCAL'] = df.apply(lambda row : parseDate(row),axis=1)
    df = df.sort_values(by=['LABEL','LOGDT_LOCAL']).reset_index(drop=True)

    # Step 3 - route process and route correction.
    df = route_process(df)
    df = route_measure_correction(df)

    # Step 4  - Direction process
    print('Missing records are: ', len(df[df['routeId'] != df['END_ROUTEID']]))
    df = df[df['routeId'] == df['END_ROUTEID']]
    # df = direction_process(df)
    # df = direction_measure_correction(df)
    # df = df.drop(['route_correction', 'direction_correction'], axis=1).reset_index(drop=True)
    # print('Total route corrections {}, total direction corrections {}'.format(rc,dc))
    return df


def get_routeId(measures):
    if not measures:
        return [None,None]
    measures = sorted(measures, key = lambda m : (m['distance'], dominant_route_selector(m['routeId'])))
    return [measures[0]['routeId'], measures[0]['measure']]
    # if(len(measures) < 1):
    #     return [None,None]
    # df2 = pd.DataFrame(measures)
    # df2['route_class'] = df2.routeId.str[0]
    # df2['ramp'] = 1
    # df2.loc[(pd.isnull(df2.routeId.str[11])), 'ramp'] = 0
    # df2['priority'] = 1  # low
    # df2.loc[(df2.distance <= 20) & (df2.ramp == 0), 'priority'] = 0
    # df2 = df2.sort_values(by=['distance']).reset_index(drop=True)
    # if df2.loc[0, 'ramp'] == 1:
    #     df2 = df2.sort_values(by=['priority', 'distance']).reset_index(drop=True)
    # return [df2.loc[0,'routeId'], df2.loc[0,'measure']]


def route_correction(a):
    a['route_class'] = a.routeId.str[0:10]
    a['END_ROUTECLASS'] = a.END_ROUTEID.str[0:10]
    a.loc[~pd.isnull(a.routeId.str[11]), 'route_class'] = 'R'
    a.loc[~pd.isnull(a.END_ROUTEID.str[11]), 'END_ROUTECLASS'] = 'R'
    a['diff'] = 0
    a.loc[a['END_ROUTECLASS'] != a['route_class'], 'diff'] = 1
    a['NEXT_END'] = a['END_ROUTECLASS'].shift(-1)
    a['NEXT_LABEL'] = a['LABEL'].shift(-1)
    index = a.index[(a['diff'] == 1) & (a['route_class'] == a['NEXT_END']) & (a['NEXT_LABEL'] == a['LABEL'])]

    a.loc[index, 'END_ROUTEID'] = a.loc[index, 'routeId']
    a.loc[index, 'END_ROUTECLASS'] = a.loc[index, 'route_class']

    a.loc[index + 1, 'routeId'] = a.loc[index + 1, 'END_ROUTEID']
    a.loc[index + 1, 'route_class'] = a.loc[index + 1, 'END_ROUTECLASS']
    a['route_correction'] = 0
    a.loc[index, 'route_correction'] = 1
    a.loc[index + 1, 'route_correction'] = 1
    a = a.loc[a['route_class'] == a['END_ROUTECLASS']]
    a = a.drop(['END_ROUTECLASS', 'route_class', 'NEXT_END', 'NEXT_LABEL', 'diff'], axis=1)
    return a


def route_process(a):
    a = a[pd.notnull(a['routeId'])]
    a = a.sort_values(by=['LABEL', 'LOGDT_LOCAL']).reset_index(drop=True)
    a['END_LABEL'] = a['LABEL'].shift(-1)
    a['END_LATI'] = a['YPOSITION'].shift(-1)
    a['END_LONG'] = a['XPOSITION'].shift(-1)
    a['END_DT'] = a['LOGDT_LOCAL'].shift(-1).apply(lambda x: '%.0f' % x)
    a['END_ROUTEID'] = a['routeId'].shift(-1)
    a['END_MEASURE'] = a['measure'].shift(-1)
    a = a.loc[a['LABEL'] == a['END_LABEL']]
    a = route_correction(a)
    return a


def get_measure_w_route_start(df):
    increment_rc()
    locations = [[df['XPOSITION'], df['YPOSITION'],df['routeId'],None]]
    gm = dbaccess.geometryToMeasures(locations,50,4326)
    if len(gm[0]) > 0:
        return gm[0][0]['measure']
    else:
        return None


def get_measure_w_route_end(df):
    increment_rc()
    locations = [[df['END_LONG'], df['END_LATI'],df['END_ROUTEID'],None]]
    gm = dbaccess.geometryToMeasures(locations, 50, 4326)
    if len(gm[0]) > 0:
        return gm[0][0]['measure']
    else:
        return None


def route_measure_correction(a):
    a.loc[a['route_correction'] == 1, 'measure'] = a.loc[a['route_correction'] == 1].apply(get_measure_w_route_start,
                                                                                           axis=1)
    a.loc[a['route_correction'] == 1, 'END_MEASURE'] = a.loc[a['route_correction'] == 1].apply(get_measure_w_route_end,
                                                                                               axis=1)
    return a


def dir_name(x):
    if x==1:
        return 'E'
    elif x==-1:
        return 'W'
    elif x==2:
        return 'N'
    else:
        return 'S'

def dir_id(x):
    if x[10]=='E':
        return 1
    elif x[10]=='W':
        return -1
    elif x[10]=='N':
        return 2
    else:
        return -2


def direction_process(a):
    a['dir_id'] = a['routeId'].apply(dir_id)
    a['opposite'] = (-1) * a['dir_id']
    a['OPPOSITE_ROUTE'] = a.routeId.str[0:10] + a['opposite'].apply(dir_name)
    a['direction_correction'] = 1
    a.loc[(a['measure'] <= a['END_MEASURE']) & (a['routeId'] == a['END_ROUTEID']), 'direction_correction'] = 0
    a = a.drop(['dir_id', 'opposite'], axis=1)
    a.loc[(a['direction_correction'] == 1) & ~pd.isnull(a.routeId.str[11]), 'direction_correction'] = 0  # exclude ramps
    # Correct directions only if the opposite route exists.
    a.loc[a['direction_correction'] == 1, 'direction_correction'] = \
                                a.loc[a['direction_correction'] == 1].apply(lambda row : 1 if dbaccess.check_route_id_exists(row['OPPOSITE_ROUTE']) else 0, axis=1)
    return a



def get_measure_w_direction_start(df):
    increment_dc()
    locations = [[df['XPOSITION'], df['YPOSITION'], df['routeId'], None]]
    gm = dbaccess.geometryToMeasures(locations, 50, 4326)
    if len(gm[0]) > 0:
        return gm[0][0]['measure']
    else:
        return None


def get_measure_w_direction_end(df):
    increment_dc()
    locations = [[df['END_LONG'], df['END_LATI'], df['END_ROUTEID'], None]]
    gm = dbaccess.geometryToMeasures(locations, 50, 4326)
    if len(gm[0]) > 0:
        return gm[0][0]['measure']
    else:
        return None


def direction_measure_correction(a):
    a.loc[(a['direction_correction'] == 1), 'routeId'] = a.loc[(a['direction_correction'] == 1), 'OPPOSITE_ROUTE']
    a.loc[(a['direction_correction'] == 1), 'END_ROUTEID'] = a.loc[(a['direction_correction'] == 1), 'OPPOSITE_ROUTE']

    a.loc[(a['direction_correction'] == 1), 'measure'] = \
        a.loc[a['direction_correction'] == 1].apply(get_measure_w_direction_start, axis=1)
    a.loc[(a['direction_correction'] == 1), 'END_MEASURE'] = \
        a.loc[a['direction_correction'] == 1].apply(get_measure_w_direction_end, axis=1)
    a = a.drop(['OPPOSITE_ROUTE'], axis=1)
    return a
